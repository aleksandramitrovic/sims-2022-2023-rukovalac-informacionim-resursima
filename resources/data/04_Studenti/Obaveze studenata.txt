(
   STU_VU_OZNAKA        char(2) not null,
   STU_BROJ_INDEKSA     bigint not null,
   BROJ_OBAVEZE         smallint not null,
   SLUSA_KOD            int not null,
   NP_OZNAKA            varchar(6) not null,
   OD_KADA              date,
   primary key (STU_VU_OZNAKA, STU_BROJ_INDEKSA, BROJ_OBAVEZE)
);