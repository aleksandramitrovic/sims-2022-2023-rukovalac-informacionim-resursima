def form_insert_statement(table, columns, values, primary_keys=[]):
    """
    Funkcija koja formira INSER INTO SQL upit
    :param table: naziv tabele u koju se dodaje red
    :param columns: lista kolona koje ce se popunjavati u redu
    :param values: lista vrednosti u redosledu kolona koje ce biti dodate
    :primary_keys: lista naziva kolona koje su primarni kljucevi
    :return: SQL upit ili None ako je nemoguce formirati izraz
    """
    if len(columns) != len(values):
        return None
    statement = f"INSERT INTO `{table}` "
    if len(columns) > 0:
        statement += "("
        for i in range(len(columns)):
            if columns[i] in primary_keys:
                continue # preskacemo promenu vrednosti na primarnim kljucevima sa auto-inkrementom
            statement += f"`{columns[i]}`, "
        statement = statement[:-2]
        statement += ")"
    statement += " VALUES "
    if len(values) > 0:
        statement += "("
        for i in range(len(values)):
            if columns[i] in primary_keys:
                continue # preskacemo promenu vrednosti na primarnim kljucevima sa auto-inkrementom
            statement += f"'{values[i]}', "
        statement = statement[:-2]
        statement += ")"
    return statement

def form_delete_statement(table, columns, values, operator="AND"):
    """
    Funkcija koja formira DELETE FROM SQL upit
    :param table: naziv tabele iz koje se brise
    :param columns: lista naziva kolona po kojima ce se vrsiti pretraga za zadovoljenje uslova (pronalaska reda)
    :param values: lista vrednosti na kolonama koje moraju odgovarati redu
    :return: SQL upit ili None u slucaju da nije moguce formirati upit
    """
    if len(columns) != len(values):
        return None
    # TODO: ako ne treba operator, dozvoliti i prazan string ili None
    if operator not in ["AND", "OR", "NOT"]:
        return None
    statement = f"DELETE FROM `{table}` WHERE "
    # TODO: ukoliko su kolone i vrednosti prazne formirati DELETE bez WHERE segmenta (brisanje svih redova)
    for i in range(len(columns)):
        statement += f"`{columns[i]}`='{values[i]}' {operator} "
    statement = statement[:-len(operator)-1] # uklanjanje poslednjeg logickog operatora
    return statement

def form_update_statement(table, columns, values, primary_keys):
    """
    Funkcija koja formira UPDATE SQL upit
    :param table: naziv tabele iz koje se brise
    :param columns: lista naziva kolona koje ce menjati vrednosti
    :param values: lista vrednosti na kolonama koje ce biti postavljene (prepisane)
    :return: SQL upit ili None u slucaju da nije moguce formirati upit
    """
    if len(columns) != len(values):
        return None
    statement = f"UPDATE `{table}` SET "
    for i in range(len(columns)):
        if columns[i] in primary_keys:
            continue # preskacemo promenu vrednosti na primarnim kljucevima
        statement += f"`{columns[i]}`='{values[i]}', "
    statement = statement[:-2]
    statement += " WHERE "
    # TODO: izmeniti nastavak ako zelite da radi i po drugim kolonama filtriranje
    # za sada filter radi samo po privatnom kljucu
    for pk in primary_keys:
        position = columns.index(pk)
        statement += f"`{pk}`='{values[position]}' AND "
    statement = statement[:-4] # uklanjanje poslednjeg logickog operatora AND
    return statement

def form_select_statement(table, columns="*", order_by_columns={}):
    statement = "SELECT "
    if columns == "*":
        statement += "*"
    elif type(columns) == list:
        for column in columns:
            statement += f"`{column}`, "
        statement = statement[:-2] # uklanjanje poslednjeg zareza
    statement += f" FROM `{table}`"
    if len(order_by_columns) > 0:
        statement += " ORDER BY "
        for column, order in order_by_columns.items():
            statement += f"`{column}` {order}, "
        statement = statement[:-2] # uklanjanje poslednjeg zareza
    
    return statement

# TODO: pokusati formiranje CREATE table statement-a
def form_create_table_statemenet(table, columns, types, primary_keys=[]):
    statement = f"CREATE TABLE `{table}` "
    return statement

# SELECT statement koji dobavlja sve tabele iz seme
# Ovo je potrebno za kreiranje liste tabela u strukturi sa leve strane
def get_all_tables(schema="db"):
    return f"SELECT `table_name` FROM  information_schema.tables WHERE `table_type`='BASE TABLE' AND `table_schema`='{schema}'"
# SELECT statement koji dobvlja sve kolone iz tabele
# Ovo je potrebno za dobijanje zaglavlja tabele sa podacima
def get_all_columns(table, schema="db"):
    return f"SELECT `column_name` FROM information_schema.columns WHERE `table_name`='{table}' AND `table_schema`='{schema}'"
def get_all_columns_with_types(table, schema="db"):
    return f"SELECT `column_name`, `data_type`, `column_type` FROM information_schema.columns WHERE `table_name`='{table}' AND `table_schema`='{schema}'"
# SELECT statement koji dobavlja primarne kljuceve iz tabele
# Ovo je potrebno za prevenciju izmene primarnih kljuceva u dijalozima
# ili za biranje koji strani kljuc moze biti u drugoj tabeli
def get_primary_keys(table, schema="db"):
    return f"SELECT `column_name` FROM information_schema.table_constraints JOIN information_schema.key_column_usage \
        USING (`constraint_name`, `table_schema`, `table_name`) WHERE `constraint_type`='PRIMARY KEY' AND `table_schema`='{schema}' \
            AND `table_name`='{table}'"
# SELECT statement koji dobavlja kolone koje su not null
# Ovo je potrebno kako bi se trazilo da neke kolone imaju obavezne vrednosti
def get_not_null_columns(table, schema="db"):
    return f"SELECT `column_name` FROM information_schema.columns WHERE `IS_NULLABLE`='No' AND `table_name`='{table}' AND `table_schema`='{schema}'"
# SELECT statement koji dobavlja kolone koje imaju auto-inkrement
# Ovo je potrebno da bi se prilikom kreiranja izostavili podaci koji su auto-inkrement
def get_autoincrement_columns(table, schema="db"):
    return f"SELECT `column_name` FROM information_schema.columns WHERE `table_name`='{table}' AND `table_schema`='{schema}' AND EXTRA LIKE '%auto_increment%'"
