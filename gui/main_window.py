from PySide2.QtWidgets import QMainWindow, QLabel
from PySide2.QtGui import QIcon
from PySide2.QtCore import Qt
from gui.widgets.central_widget import CentralWidget
from gui.widgets.menu_bar import MenuBar
from gui.widgets.status_bar import StatusBar
from gui.widgets.tool_bar import ToolBar
from gui.widgets.workspace_widget.workspace_widget import WorkspaceWidget


class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        # poziv super inicijalizaotra (QMainWindow)
        super().__init__(parent)
        # Osnovna podesavanja glavnog prozora
        self.setWindowTitle("Rukovalac informacionim resursima")
        self.setWindowIcon(QIcon("resources/icons/blue-document.png"))
        self.resize(1000, 800)

        # inicijalizacija osnovnih elemenata GUI-ja
        self.tool_bar = ToolBar(parent=self)
        self.menu_bar = MenuBar(self)
        self.status_bar = StatusBar(self)
        self.central_widget = CentralWidget(self)
        self.workspace_widget = WorkspaceWidget("Struktura radnog prostora", self)

        # FIXME: obrisati nakon testiranja
        self.populate_workspace()

        # uvezivanje elemenata GUI-ja
        self.setMenuBar(self.menu_bar)
        self.addToolBar(self.tool_bar)
        self.setStatusBar(self.status_bar)
        self.setCentralWidget(self.central_widget)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.workspace_widget, Qt.Vertical)

        # sacuvavanje prijavljenog korisnika iz dijaloga
        self.status_bar.addWidget(QLabel("Ulogovani korisnik: " + "Aleksandra Mitrovic"))

    def get_actions(self):
        return self.tool_bar.actions_dict
    
    def add_actions(self, where="menu", name=None, actions=[]):
        if where == "menu":
            # dodati u meni sa nazivom name (file, edit...)
            self.menu_bar.add_actions(name, actions)
        elif where == "toolbar":
            # dodati akcije u toolbar nakon separatora
            self.tool_bar.addSeparator()
            self.tool_bar.addActions(actions)
       

    # FIXME: samo zarad demonstracije
    def populate_workspace(self):
        from gui.widgets.workspace_widget.model.workspace import Workspace
        from gui.widgets.workspace_widget.model.collection import Collection
        from gui.widgets.workspace_widget.model.file import File

        # ws
        #    c1
        #       c2
        #           f1
        #           f2
        #    c3
        ws = Workspace("Workspace", "./", [])
        c1 = Collection("Collection 1", "./", "./", ws, [])
        c2 = Collection("Collection 2", "./", "./", c1, [])
        c3 = Collection("Collection 3", "./", "./", ws, [])
        f1 = File("File 1", "./", "./", c2)
        f2 = File("File 2", "./", "./", c2)

        ws.collections.append(c1)
        ws.collections.append(c3)

        c1.content.append(c2)
        c2.content.append(f1)
        c2.content.append(f2)

        self.workspace_widget.set_workspace(ws)



