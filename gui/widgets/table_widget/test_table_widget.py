from PySide2.QtWidgets import QWidget, QVBoxLayout, QTableWidget, QTableWidgetItem


class TestTableWidget(QWidget):
    # kompozicijom (pravljenje slozenog widgeta, na osnovu jednostavnih)
    def __init__(self, parent=None):
        super().__init__(parent)
        # layout
        self.widget_layout = QVBoxLayout()

        # populisanje layout-a
        self.table_widget = QTableWidget(parent)
        self.table_widget.setColumnCount(3) # indeks, ime, prezime
        self.table_widget.setRowCount(10)
        self.table_widget.setHorizontalHeaderLabels(["Indeks", "Ime", "Prezime"])

        self.fill_data() # popunjavanje tabele

        self.widget_layout.addWidget(self.table_widget)
        self.setLayout(self.widget_layout) # odmah setujemo layout

    def fill_data(self):
        for i in range(10):
            # item za kolonu 1
            col1 = QTableWidgetItem("2702"+ str(i))
            col2 = QTableWidgetItem("Marko" + str(i))
            col3 = QTableWidgetItem("Markovic" + str(i))

            # umetanje item-a na zadatu celiju tabele
            self.table_widget.setItem(i, 0, col1)
            self.table_widget.setItem(i, 1, col2)
            self.table_widget.setItem(i, 2, col3)

    