from PySide2.QtWidgets import QDialog, QDialogButtonBox, QComboBox, QFormLayout, QVBoxLayout
from PySide2.QtGui import QIcon

class TableSelectionDialog(QDialog):
    def __init__(self, parent=None):
        # parent nam treba za kreiranje modalnog dijaloga
        super().__init__(parent)

        self.setWindowTitle("Select table from schema")
        self.setWindowIcon(QIcon("resources/icons/database.png"))
        self.resize(400, 200)

        self.dialog_layout = QVBoxLayout()
        self.form_layout = QFormLayout()

        self.tables_combobox = QComboBox()

        # dugmici
        self.button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)

        # uvezivanje funkcija koje ce se pozivati kada se okine odredjeni dugmic
        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)

        self.form_layout.addRow("Table name:", self.tables_combobox)

        self.dialog_layout.addLayout(self.form_layout)
        self.dialog_layout.addWidget(self.button_box)

        self.setLayout(self.dialog_layout)

    def populate_tables(self, tables):
        self.tables_combobox.addItems(tables)

    def get_data(self):
        return self.tables_combobox.currentText()