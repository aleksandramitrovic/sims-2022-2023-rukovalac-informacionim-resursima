from PySide2.QtWidgets import QWidget, QVBoxLayout, QTabWidget, QFileDialog, QAction, QMessageBox
from PySide2.QtGui import QIcon
# from gui.widgets.table_widget.test_table_widget import TestTableWidget
from gui.widgets.data_view.data_view import DataView
from gui.widgets.data_view.data_model import DataModel
from gui.widgets.database_dialog.database_dialog import DatabaseDialog
from gui.widgets.database_dialog.table_selection_dialog import TableSelectionDialog
from core.database.connection import *
from core.database.statements import *



class CentralWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.on_open_action = QAction(QIcon("resources/icons/folder-open-document.png"), "Open")
        self.on_open_action.triggered.connect(self.open_primary_table)
        self.open_database_action = QAction(QIcon("resources/icons/database.png"), "Open database")
        self.open_database_action.triggered.connect(self.on_open_database_action)
        # open table action se moze proslediti drugom widgetu
        self.open_table_action = QAction(QIcon("resources/icons/database-property.png"), "Open table")
        self.open_table_action.triggered.connect(self.on_open_table_action)

        self.cw_layout = QVBoxLayout(self)

        self.primary_tables_tab_widget = QTabWidget(self)
        self.secondary_tables_tab_widget = QTabWidget(self)

        self.primary_tables_tab_widget.setTabsClosable(True)
        self.secondary_tables_tab_widget.setTabsClosable(True)

        self.primary_tables_tab_widget.tabCloseRequested.connect(self.on_close_primary_tab)
        self.secondary_tables_tab_widget.tabCloseRequested.connect(self.secondary_tables_tab_widget.removeTab)

        self.cw_layout.addWidget(self.primary_tables_tab_widget)
        self.cw_layout.addSpacing(15)
        self.cw_layout.addWidget(self.secondary_tables_tab_widget)

        # obavezno dati layout uvezujemo na widget (CentralWidget)
        self.setLayout(self.cw_layout)

        # FIXME: samo radi prikaza, ovo smestiti u konfiguraciju
        # podaci o konekciji
        self.connection_data = None
        self.data_source_type = None

        self.parent().add_actions(where="toolbar", name=None, actions=self.export_actions())

    def open_primary_table(self):
        
        # 1. pronaci datoteka/folder workspace-a koji se otvara
        # 2. parsirati pomocu handle metode i kreirati model
        # 3. napraviti DataView (tabelarni prikaz)
        # 4. postaviti model DataView-u
        # 5. kreirati novi Tab u koji ce kao widget biti dodat DataView
        # 6. pronaci sve zavisnosti i kreirati nove tabove 
        # u secondary_tab_widget (koraci isti kao i za primary_tab_widget, koraci 2-5)
        # FIXME: demonstrativni primer, promeniti po sledecim koracima
        from gui.widgets.data_view.data_model import DataModel
        import csv
        file_name = QFileDialog.getOpenFileName(self, "Open data file", "/resources/data", "CSV files (*.csv)")
        # kada se otvori diajlog a ne izabere datoteka
        if file_name[0] == "":
            return
        # ucitavanje podataka iz datoteke
        with open(file_name[0], "r", encoding="utf-8") as fp:
            reader = csv.reader(fp, delimiter=";")
            reader = list(reader)
            header = reader[0] # FIXME: ovo ce biti u meta-podacima
            data = reader[1:]
            # kreiranje modela na osnovu ucitanih podataka
            table_model = DataModel(data=data, header_data=header)
            # postavljanje modela u prikaz
            data_view = DataView()
            data_view.table_view.setModel(table_model)
            self.primary_tables_tab_widget.addTab(data_view, file_name[0].split("/")[-1])
        
    def export_actions(self):
        return [self.on_open_action, self.open_database_action, self.open_table_action]
    

    def on_close_primary_tab(self, index):
        # TODO ako ima nekih promena pitati da li se zele one sacuvati itd.
        QMessageBox.warning(self.parent(), "Upozorenje za cuvanje podataka", "Podaci nisu sacuvani, ako zelite prvo ih sacuvajte pa zatvorite tab", QMessageBox.Ok)
        self.primary_tables_tab_widget.removeTab(index)

    def on_open_database_action(self):
        database_dialog = DatabaseDialog(self)
        result = database_dialog.exec_()
        if result == 1:
            self.connection_data = database_dialog.get_data()
            connection = open_connection("localhost", self.connection_data["username"], self.connection_data["password"],  self.connection_data["db_name"])
            if connection is not None:
                QMessageBox.information(self, "Connection to a database", "Connection to database " + self.connection_data["db_name"] + " is successful!")

    def on_open_table_action(self):
        if self.connection_data is None:
            QMessageBox.warning(self, "Connection failed", "First connect to a database to search for tables!")
            return
        table_selection_dialog = TableSelectionDialog(self)
        connection = open_connection("localhost", self.connection_data["username"], self.connection_data["password"],  self.connection_data["db_name"])
        sql_tables = execute_statement(connection, get_all_tables(schema=self.connection_data["db_name"]))
        tables = []
        for table in sql_tables:
            tables.append(table["TABLE_NAME"])
        table_selection_dialog.populate_tables(tables)
        result = table_selection_dialog.exec_()
        if result == 1:
            table = table_selection_dialog.get_data()
            connection = open_connection("localhost", self.connection_data["username"], self.connection_data["password"],  self.connection_data["db_name"])
            select_statement = form_select_statement(table)
            # FIXME: ako tabela u bazi nema redova, javlja se greska
            data = execute_statement(connection, select_statement)
            print(data)
            # TODO: popuniti model za prikaz u glavnom widget-u
            # kreiranje modela na osnovu ucitanih podataka
            # FIXME: dobaviti koje se kolone prikazuju za header
            table_model = DataModel()
            table_model.from_dict(data)
            self.data_source_type = "database"
            self.connection_data["table"] = table

            data_view = DataView()
            data_view.data_source_type = "database"
            data_view.table_view.setModel(table_model)
            self.primary_tables_tab_widget.addTab(data_view, table)
