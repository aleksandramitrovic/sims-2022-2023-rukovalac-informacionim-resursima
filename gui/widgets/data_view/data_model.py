from PySide2.QtCore import QAbstractTableModel, QModelIndex, Qt

class DataModel(QAbstractTableModel):
    def __init__(self, parent=None, header_data=None, data=None):
        super().__init__(parent)
        self.table_data = data
        self.header_data = header_data

    def get_headers(self):
        return self.header_data
    
    def get_row(self, row=0):
        # metoda sluzi za dobavljanje podataka iz reda
        # ako je prosledjeni red u okviru granica tabele (njenog broja redova)
        if self.table_data is not None:
            if row < len(self.table_data):
                data_row = self.table_data[row] # lista elemenata
                if data_row:
                    return data_row
                
    def replace_data(self, row, data=[]):
        # emitovati signal za promenu
        # izvrsiti promenu
        self.table_data[row] = data
        # emitovati signal za kraj promene

    # sopstvena metoda (ne redefinise se)
    def get_element(self, index: QModelIndex):
        """
        :param index: indeks sa kojeg se dobavlja element (sadrzaj)
        :return: sadrzaj za datu celiju / cela lista (u slucaju da je indeks nevalidan)
        """
        if index.isValid():
            # element iz matrice dobijamo spram reda i kolone indeksa
            element = self.table_data[index.row()][index.column()]
            if element:
                return element
        return self.table_data
    
    def from_dict(self, data):
        """
        Metoda koja na osnovu liste recnika pravi model podataka
        :param data: lista recnika
        """
        if len(data) > 0:
            # smestamo kljuceve sa prvog recnika
            self.header_data = list(data[0].keys())
            self.table_data = []
            for row in data:
                # row je recnik od kojeg uzimamo vrednosti da bismo kreirali red u tabeli
                # self.table_data.append(list(row.values()))
                values = []
                for value in list(row.values()):
                    # FIXME: ne pretvarati sve kolone u string
                    # vec spram tipa podatka u bazi podataka ucitati odgovarajuci tip
                    values.append(str(value))
                self.table_data.append(values)

    # redefinisanje neophodnih metoda za pravljenje naseg modela
    def rowCount(self, parent=...):
        return len(self.table_data) # koliko ima redova (podlista) u listi
    
    def columnCount(self, parent=...):
        return len(self.header_data)
    
    def data(self, index, role=...):
        element = self.get_element(index)
        if role == Qt.DisplayRole: # Qt.DecorationRole (za ikonice)
            return element # sadrzaj celije
        
    def headerData(self, section: int, orientation: Qt.Orientation, role: int = ...):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.header_data[section] # section == kolona
        elif orientation == Qt.Vertical:
            return super().headerData(section, orientation, role)
