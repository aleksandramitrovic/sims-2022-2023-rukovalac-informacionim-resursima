from PySide2.QtWidgets import QWidget, QVBoxLayout, QTableView, QAction, QFileDialog, QAbstractItemView, QMessageBox
from PySide2.QtGui import QIcon
from gui.widgets.data_view.data_model import DataModel
from gui.widgets.edit_dialog.edit_dialog import EditDialog
import csv
from core.database.connection import *
from core.database.statements import *


class DataView(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.widget_layout = QVBoxLayout()

        # definisanje akcije za otvaranje datoteke
        # FIXME: nadleznost widget-a u kojem se prikazuje workspace
        self.on_open_file_action = QAction(QIcon("resources/icons/folder-open-document.png"), "Open")
        self.on_open_file_action.triggered.connect(self.on_open_file)
        
        self.table_view = QTableView()
        self.table_view.setSelectionMode(QAbstractItemView.SingleSelection)
        self.table_view.setSelectionBehavior(QAbstractItemView.SelectRows)

        # uvezivanje da kada se dvoklikne na red se otvori dijalog za izmenu podataka
        self.table_view.doubleClicked.connect(self.open_edit_form)

        # Model ce se populisati kada se otvori datoteka
        # self.table_model = DataModel()
        # self.table_view.setModel(self.table_model)

        self.widget_layout.addWidget(self.table_view)
        self.setLayout(self.widget_layout)

    def export_actions(self):
        return [self.on_open_file_action]

    # FIXME: prebaciti u cental_widget ili u dock
    def on_open_file(self):
        # izbor datoteke iz fajl sistema
        file_name = QFileDialog.getOpenFileName(self, "Open data file", "/resources/data", "CSV files (*.csv)")
        # kada se otvori diajlog a ne izabere datoteka
        if file_name[0] == "":
            return
        # ucitavanje podataka iz datoteke
        with open(file_name[0], "r", encoding="utf-8") as fp:
            reader = csv.reader(fp, delimiter=";")
            reader = list(reader)
            header = reader[0] # FIXME: ovo ce biti u meta-podacima
            data = reader[1:]
            # kreiranje modela na osnovu ucitanih podataka
            self.table_model = DataModel(data=data, header_data=header)
            # postavljanje modela u prikaz
            self.table_view.setModel(self.table_model)
            self.parent().parent().parent().data_source_type = "file"

    def open_edit_form(self, index=None):
        model = self.table_view.model()
        selected_labels = model.get_headers()
        selected_data = model.get_row(index.row())
        # kreiranje dijaloga
        edit_dialog = EditDialog(self.parent())
        # populisanje dialoga podacima iz reda tabele
        if self.parent().parent().parent().data_source_type == "file":
            edit_dialog.enter_data(selected_labels, selected_data)
        elif self.parent().parent().parent().data_source_type == "database":
            primary_keys = []
            connection_data = self.parent().parent().parent().connection_data
            connection = open_connection("localhost", connection_data["username"], connection_data["password"],  connection_data["db_name"])
            data = execute_statement(connection, get_primary_keys(connection_data["table"], connection_data["db_name"]))
            for pk in data:
                primary_keys.append(pk["column_name"])
            not_nullable = []
            connection_data = self.parent().parent().parent().connection_data
            connection = open_connection("localhost", connection_data["username"], connection_data["password"],  connection_data["db_name"])
            data = execute_statement(connection, get_not_null_columns(connection_data["table"], connection_data["db_name"]))
            for nn in data:
                not_nullable.append(nn["COLUMN_NAME"])
            edit_dialog.enter_data(selected_labels, selected_data, read_only=primary_keys, not_null=not_nullable)
        # prikazivanje dijaloga
        result = edit_dialog.exec_()
        if result == 1: # izmena prihvacena
            data = edit_dialog.get_data()
            model.replace_data(index.row(), data)
            # TODO: dodati da se prosledi SQL upit ukoliko je tabela iz baze
            if self.parent().parent().parent().data_source_type == "database":
                connection_data = self.parent().parent().parent().connection_data
                connection = open_connection("localhost", connection_data["username"], connection_data["password"],  connection_data["db_name"])
                execute_statement(connection, form_update_statement(connection_data["table"], selected_labels, data, primary_keys))



