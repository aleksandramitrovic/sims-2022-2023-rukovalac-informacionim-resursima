from PySide2.QtWidgets import QDialog, QVBoxLayout, QFormLayout, QDialogButtonBox, QLineEdit, QMessageBox
from PySide2.QtGui import QIcon


class EditDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setWindowTitle("Edit data")
        self.setWindowIcon(QIcon("resources/icons/pencil.png"))
        self.resize(400, 300)

        self.dialog_layout = QVBoxLayout()
        self.form_layout = QFormLayout()

        # dugmici
        self.button_box = QDialogButtonBox(QDialogButtonBox.Save | QDialogButtonBox.Cancel)

        # uvezivanje funkcija koje ce se pozivati kada se okine odredjeni dugmic
        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)

        # populisanje layout-a
        self.dialog_layout.addLayout(self.form_layout)
        self.dialog_layout.addWidget(self.button_box)
        
        self.setLayout(self.dialog_layout)

    def enter_data(self, labels=[], data=[], read_only=[], not_null=[]):
        """
        Upisuje podatke u LineEdit widget-e na osnovu sadrzaja reda tabele koja je selektovana
        :param labels: nazivi kolona (labela) koje ce se prikazati
        :param data: vrednosti koje ce se upisati u input-e (LineEdit)
        :param read_only: nazivi kolona koje se ne mogu menjati kroz dijalog (primarni kljucevi)
        """
        self.not_null_labels = not_null
        self.read_only_labels = read_only
        # metoda pravi redove u formi tako da su labele iz liste labela
        # a na odgovarajucim pozicijama ce se dobaviti i podaci iz liste data
        if len(data) == 0:
            # ako je lista podataka prazna, samo napraviti prazan sadrzaj
            for i in range(len(labels)):
                # FIXME: spram tipa podatka kolone (labele), dodati odgovarajuci EDIT widget (spinbox, datetimeedit, ...)
                line_edit = QLineEdit()
                if labels[i] in read_only:
                    line_edit.setReadOnly(True)
                self.form_layout.addRow(labels[i], line_edit)
        else:
            for i in range(len(labels)):
                line_edit = QLineEdit(str(data[i]))
                if labels[i] in read_only:
                    line_edit.setReadOnly(True)
                self.form_layout.addRow(labels[i], line_edit)

    def get_data(self):
        # metoda koja iscitava podatke iz forme i vraca kao rezultat
        data = []
        for row_number in range(1, self.form_layout.rowCount()*2, 2):
            # prikupljanje teksta iz lineedit-a
            data.append(self.form_layout.itemAt(row_number).widget().text())
        return data
    
    def check_data(self, not_null=[]):
        # prolazimo kroz redove forme i gledamo samo labele
        for row_number in range(0, self.form_layout.rowCount()*2, 2):
            # ako labela ima naziv kao iz not_null, njen LineEdit mora imati sadrzaj
            label = self.form_layout.itemAt(row_number).widget()
                                            # FIXME: za druge EDIT widget-e proveriti da li je samo provera text() sadrzaja
            if label.text() in not_null and self.form_layout.itemAt(row_number+1).widget().text() == "":
                QMessageBox.warning(self, "Warning", "Field " + label.text() + " must have a value!", QMessageBox.Ok)
                return False
        return True
    
    def accept(self):
        result = self.check_data(self.not_null_labels)
        if result:
            return super().accept()
        return