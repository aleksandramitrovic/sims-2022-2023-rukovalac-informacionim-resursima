from PySide2.QtCore import QAbstractItemModel, QModelIndex, Qt
from PySide2.QtGui import QIcon


class WorkspaceModel(QAbstractItemModel):
    """
    Klasa koja predstavlja model strukture workspace-a organizacije tipa stabla
    """
    def __init__(self, parent=None, workspace=None):
        super().__init__(parent)
        # korenski element instanca klase Wokrspace (iz paketa model)
        self.workspace = workspace

    def get_element(self, index: QModelIndex):
        """
        :param index: indeks sa kojeg se dobavlja element (sadrzaj)
        :return: sadrzaj za iz hijerarhije workspace-a/ workspace (u slucaju da je indeks nevalidan)
        """
        if index.isValid():
            # element iz matrice dobijamo spram reda i kolone indeksa
            element = index.internalPointer()
            if element:
                return element
        return self.workspace

    # metode neophodne za READ_ONLY model
    def index(self, row, column, parent=QModelIndex()):
        item = self.get_element(parent)
        if ((row >= 0) and (row < len(item.children())) and item.children()[row]):
            return self.createIndex(row, column, item.children()[row])
        return QModelIndex() # nevalidan indeks
    
    def parent(self, index):
        """
        Vraca indeks roditelja od indeksa deteta koji je prosledjen
        :param index: indeks deteta
        :return: indeks roditelja od prosledjenog deteta
        """
        if index.isValid():
            child_element = self.get_element(index)
            parent_element = child_element.parent()
            if parent_element and (parent_element != self.workspace): #  and parent_element.parent()
                # koji je po redu parent element medju ostalom decom svog roditelja
                row = parent_element.parent().children().index(parent_element)
                column = 0
                obj = parent_element
                return self.createIndex(row, column, obj)
        return QModelIndex()

    def rowCount(self, parent):
        item = self.get_element(parent)
        if item is None or item.children() is None:
            return 0
        return len(item.children())

    
    def columnCount(self, parent):
        return 1 # samo naziv
    
    def data(self, index, role):
        """
        Dobavlja sadrzaj na datom indeksu za prosledjenu ulogu
        :param index: indeks za koji se dobavlja sadrzaj (ikonica ili tekst)
        :param role: za koju ulogu se prikazu podaci (obradjujemo samo uloge ikonice i teksta)
        :return: tekst ili ikonica
        """
        item = self.get_element(index)

        if (index.column() == 0) and (role == Qt.DecorationRole):
            # TODO: uraditi proveru tipa podatka, pa spram toga izabrati adekvatnu ikonicu
            return QIcon("resources/icons/blue-document.png")
        elif (index.column() == 0) and (role == Qt.DisplayRole):
            return item.name

    def headerData(self, section, orientation, role):
        if orientation != Qt.Vertical:
            if (section == 0) and (role == Qt.DisplayRole):
                return "Name"
    # ====================================