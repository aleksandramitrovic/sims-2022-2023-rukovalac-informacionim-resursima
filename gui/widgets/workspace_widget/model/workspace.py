from gui.widgets.workspace_widget.model.information_resource import InfromationResource

class Workspace(InfromationResource):
    def __init__(self, name="", path="", collections=[]):
        super().__init__(name, path)
        self.collections = collections

    def parent(self):
        return None
    
    def children(self):
        return self.collections
    
    def handle(self):
        # TODO:
        return None
        