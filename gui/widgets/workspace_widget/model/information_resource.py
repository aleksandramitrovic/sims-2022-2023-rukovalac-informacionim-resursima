from abc import ABC, abstractmethod
from gui.widgets.workspace_widget.model.type_handler import TypeHanlder

class InfromationResource(TypeHanlder, ABC):
    def __init__(self, name="", path=""):
        super().__init__()
        # definisati atribute koji su zajednicki radni prostor, kolekcija, datoteka
        self.name = name
        self.path = path

    # dekorator
    @abstractmethod
    def parent(self): ...

    @abstractmethod
    def children(self):
        """
        Metoda koja vraca listu dece informacionog resursa
        :return: lista prazna ili popunjena decom
        """
        ...