from gui.widgets.workspace_widget.model.information_resource import InfromationResource


class Collection(InfromationResource):
    def __init__(self, name="", path="", metadata_path="", parent=None, content=[]):
        super().__init__(name, path)
        self.metadata_path = metadata_path
        # workspace ili collection kojem ova kolekcija pripada
        self._parent = parent
        # sadrzaj je ili kolekcija iskljucivo drugi kolekcija ili kolekcija fajlova
        self.content = content

    def parent(self):
        return self._parent
    
    def children(self):
        return self.content

    def handle(self):
        # TODO:
        return None