from gui.widgets.workspace_widget.model.information_resource import InfromationResource


class File(InfromationResource):
    def __init__(self, name="", path="", metadata_path="", collection=None):
        super().__init__(name, path)
        self.metadata_path = metadata_path
        self.collection = collection

    def parent(self):
        return self.collection
    
    def children(self):
        # fajl je listni cvor
        return []
    
    def handle(self):
        # TODO:
        return None