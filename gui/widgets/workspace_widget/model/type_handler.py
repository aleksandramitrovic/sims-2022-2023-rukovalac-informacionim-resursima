from abc import ABC, abstractmethod

# "interfejs" za obradu podataka (parsiranje) informacionih resursa
class TypeHanlder(ABC):

    @abstractmethod
    def handle(self):
        ...