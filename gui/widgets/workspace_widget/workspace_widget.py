from PySide2.QtWidgets import QDockWidget, QTreeView
from gui.widgets.workspace_widget.workspace_model import WorkspaceModel

class WorkspaceWidget(QDockWidget):
    def __init__(self, title="Stucture", parent=None):
        super().__init__(title, parent)
        self.structure_widget = QTreeView()
        self.structure_model = WorkspaceModel()
        self.structure_widget.setModel(self.structure_model)

        self.setWidget(self.structure_widget)

    def set_workspace(self, workspace):
        self.structure_model = WorkspaceModel(None, workspace)
        self.structure_widget.setModel(self.structure_model)

